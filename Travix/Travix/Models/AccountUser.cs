﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Travix.Models
{
    public class AccountUser
    {
        [Required(ErrorMessage="Please enter Login")]
        [StringLength(50)]
        public string Login { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [RegularExpression(@"^(?=.*[a-z]).{3,8}$")] // should be some strong regex
        public string Password { get; set; }
    }
}