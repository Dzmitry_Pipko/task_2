﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Travix.Models;
using DomainModel = BE.DomainModels;
using EntityModel = DAL.EntityModels;

namespace Travix.ModelConverter
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            ConfigureUserMapping();
            ConfigureBlogMapping();

            Mapper.CreateMap<AccountUser, DomainModel.User>();
        }

        private static void ConfigureUserMapping()
        {
            Mapper.CreateMap<EntityModel.User, DomainModel.User>();
            Mapper.CreateMap<DomainModel.User, EntityModel.User>();
        }

        private static void ConfigureBlogMapping()
        {
            Mapper.CreateMap<EntityModel.Blog, DomainModel.Blog>();
            Mapper.CreateMap<DomainModel.Blog, EntityModel.Blog>();
        }
    }
}