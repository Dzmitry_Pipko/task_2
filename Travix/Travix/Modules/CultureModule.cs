﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Travix.Helpers;

namespace Travix.Modules
{
    public class CultureModule : IHttpModule
    {
        public void Init(HttpApplication app)
        {
            app.PreRequestHandlerExecute += Application_BeginRequest;
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;

            string cultureNameFromUrl = context.Request.RequestContext.RouteData.Values["lang"] as string;
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = context.Request.Cookies["_culture"];

            if (cultureNameFromUrl != null)
                cultureName = cultureNameFromUrl;
            else if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = context.Request.UserLanguages != null && context.Request.UserLanguages.Length > 0 ? context.Request.UserLanguages[0] :  null;

            cultureName = CultureHelper.GetImplementedCulture(cultureName); 

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }

        public void Dispose()
        {
        }
    }
}