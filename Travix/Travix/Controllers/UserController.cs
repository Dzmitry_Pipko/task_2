﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services.Description;
using DAL.Abstractions;
using AutoMapper;
using BE.DomainModels;
using BLL.Abstractions;
using BLL.Services;
using DAL.UnitOfWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Ninject;
using Travix.Models;

namespace Travix.Controllers
{
    public class UserController : Controller
    {

        //
        // GET: /User/

        private readonly IUserService _iUserService;
        
        public UserController(IUserService iUserService)
        {
            _iUserService = iUserService;
        }

        [Authorize]
        public JsonResult Index()
        {
            var currentUser = _iUserService.GetUserByLogin(User.Identity.Name);

            if (currentUser == null)
                return null;

            return Json(currentUser, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult SignIn(AccountUser user, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                RedirectToAction("Index", "Home");
            
            if (!ModelState.IsValid)
                // If we got this far, something failed, redisplay form
                return View("SignIn");

            var domainUser = Mapper.Map<AccountUser, User>(user);
            domainUser.Login = Server.HtmlDecode(domainUser.Login);

            bool isExist;

            using (var unitOfWork = (IUnitOfWork)DependencyResolver.Current.GetService(typeof(IUnitOfWork)))
            {
                isExist = _iUserService.Login(domainUser.Login, domainUser.Password);
            }

            if (!isExist)
            {
                ModelState.AddModelError("CustomMessage", "The user name or password provided is incorrect.");
                return View("SignIn");
            }

            FormsAuthentication.SetAuthCookie(domainUser.Login, false);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SingOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}
