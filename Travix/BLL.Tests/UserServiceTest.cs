﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DAL.Abstractions;
using BE.DomainModels;
using BLL.Helper;
using BLL.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;
using Travix.ModelConverter;

namespace BLL.Tests
{
    [TestClass]
    public class UserServiceTest
    {
        private IUnitOfWork unitOfWork;
        private IUserRepository userRepository;
        private const string Salt = "932DA01F-9ABD-9d9d-80C7-02AF85C02A8";

        //TODO should be created stub file with with tested data (to take data that are predefined)

        [TestInitialize]
        public void Init()
        {
            AutoMapperConfiguration.Configure();
            unitOfWork = MockRepository.GenerateStub<IUnitOfWork>();
            userRepository = MockRepository.GenerateStub<IUserRepository>();
        }

        [TestMethod]
        public void LoginShouldBeSuccess()
        {

            User testedUserDomain = new User()
            {
                Login = "testUser",
                Password = "TestPassword"
            };

            var hashedPassword = AlgorithmHelper.GenerateHashWithSalt(testedUserDomain.Password, Salt);

            userRepository.Stub(x => x.Login(testedUserDomain.Login, hashedPassword)).Return(true);

            var userService = new UserService(unitOfWork, userRepository);

            var isExist = userService.Login(testedUserDomain.Login, testedUserDomain.Password);

            Assert.IsTrue(isExist);
        }

        [TestMethod]
        public void GetByIdShouldBeSuccess()
        {
            User testedUser = new User()
            {
                Login = "testUser",
                Id = 1
            };

            userRepository.Stub(x => x.FindById(1)).Return(testedUser);

            var userService = new UserService(unitOfWork, userRepository);

            var domainUser = userService.GetById(1);
            
            Assert.AreEqual(domainUser.Login, testedUser.Login);
        }

        [TestMethod]
        public void GetByIdShouldBeIsNull()
        {
            userRepository.Stub(x => x.FindById(Arg<int>.Is.Anything)).Return(null);
            var userService = new UserService(unitOfWork, userRepository);

            var domainUser = userService.GetById(1);

            Assert.IsNull(domainUser);
        }
    }
}
