﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using Travix.ModelConverter;

namespace Travix
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfiguration.Configure();
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {

                    //should using if there is support of roling or necessary custom principal 

//                    if (HttpContext.Current.User.Identity is FormsIdentity)
//                    {
//                        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
//                        FormsAuthenticationTicket ticket = (id.Ticket);
//                        if (!FormsAuthentication.CookiesSupported)
//                        {
//                            //If cookie is not supported for forms authentication, then the
//                            //authentication ticket is stored in the URL, which is encrypted.
//                            //So, decrypt it
//                            ticket = FormsAuthentication.Decrypt(id.Ticket.Name);
//                        }
//                        // Get the stored user-data, in this case, user roles
//                        if (!string.IsNullOrEmpty(ticket.UserData))
//                        {
//                            string userData = ticket.UserData;
//                            string[] roles = userData.Split(',');
//                            //Roles were put in the UserData property in the authentication ticket
//                            //while creating it
//                                                        HttpContext.Current.User =
//                                                          new System.Security.Principal.GenericPrincipal(id, roles);
//                        }
//                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //TODO should be implemented handling error, dispocing of resources and logging
            Exception exception = Server.GetLastError();
        }
    }
}