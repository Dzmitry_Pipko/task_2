﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DAL.Abstractions;
using AutoMapper;
using DAL.EntityModels;

namespace DAL.Repositories
{
    public class UserRepository : GenericRepository<User, BE.DomainModels.User>, IUserRepository
    {
        public UserRepository(DbContext db)
            : base(db)
        {
            
        }

        public bool Login(string login, string password)
        {
            var res = FindBy(user => user.Login == login && user.Password == password);

            return res.FirstOrDefault() != null;
        }

        public BE.DomainModels.User GetUserByLogin(string login)
        {
            return Mapper.Map<User, BE.DomainModels.User>(FindBy(user => user.Login == login).FirstOrDefault());
        }
    }
}
