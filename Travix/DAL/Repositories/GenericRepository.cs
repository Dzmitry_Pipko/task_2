﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

using System.Linq.Expressions;
using System.Web;
using DAL.Abstractions;
using AutoMapper;
using DAL.EntityModels;

namespace DAL.Repositories
{
    public abstract class GenericRepository<TEntityModel, TDomainModel> : IRepository<TDomainModel>
        where TEntityModel: class
    {
        protected DbContext _entities;
        protected readonly IDbSet<TEntityModel> _dbset;

        protected GenericRepository(DbContext context)
        {
            _entities = context;
            _dbset = context.Set<TEntityModel>();
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public IEnumerable<TDomainModel> GetAll()
        {
            return Mapper.Map<IEnumerable<TEntityModel>, IEnumerable<TDomainModel>>(_dbset);
        }

        public TDomainModel FindById(int id)
        {
            return Mapper.Map<TEntityModel, TDomainModel>(_dbset.Find(id));
        }

        public IQueryable<TEntityModel> FindBy(Expression<Func<TEntityModel, bool>> predicate)
        {
            return _dbset.Where(predicate);
        }

        public TDomainModel Add(TDomainModel domainModel)
        {
            var entity = Mapper.Map<TDomainModel, TEntityModel>(domainModel);
            entity = _dbset.Add(entity);

            return Mapper.Map<TEntityModel, TDomainModel>(entity);
        }

        public bool Delete(TDomainModel domainModel)
        {
            var entity = Mapper.Map<TDomainModel, TEntityModel>(domainModel);
            _dbset.Remove(entity);
            return true;
        }

        public void Edit(TDomainModel domainModel)
        {
            var entity = Mapper.Map<TDomainModel, TEntityModel>(domainModel);
            _entities.Entry(entity).State = EntityState.Modified;
        }
    }
}
