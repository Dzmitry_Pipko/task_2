﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Abstractions;
using DAL.EntityModels;

namespace DAL.Repositories
{
    public class BlogRepository : GenericRepository<Blog, BE.DomainModels.Blog>, IBlogRepository
    {
        public BlogRepository(DbContext db)
            : base(db)
        {
           
        }
    }
}
