﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Abstractions;

namespace DAL.Repositories
{
    public class HttpCaller : IRepositoryAsync
    {
        public async Task<string> GetMessage()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:8080/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                // New code:
                HttpResponseMessage response = await client.GetAsync("DataForAsyncCall.txt");
                if (!response.IsSuccessStatusCode)
                    return string.Empty;
                return string.Empty;
//                string product = await response.Content.ReadAsAsync<string>();
//                return product;
            }
        }
    }
}
