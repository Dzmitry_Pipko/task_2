using System.Data.Entity;
using BLL.Services;
using BLL.Services.Interfaces;
using DAL.EntityModels;
using DAL.Repositories;
using DAL.Repositories.Interfaces;
using DAL.UnitOfWork;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Travix.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Travix.App_Start.NinjectWebCommon), "Stop")]

namespace Travix.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<DbContext>().To<TravixEntities>().InRequestScope();

            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();

            kernel.Bind<IBlogRepository>().To<BlogRepository>().InRequestScope();
            kernel.Bind<IRepository<Blog>>().To<GenericRepository<Blog>>().InRequestScope();

            kernel.Bind<IUserRepository>().To<UserRepository>().InRequestScope();
            kernel.Bind<IRepository<User>>().To<GenericRepository<User>>().InRequestScope();

            kernel.Bind<IService<BE.Entities.Blog>>().To<EntityService<BE.Entities.Blog, Blog>>().InRequestScope();
            kernel.Bind<IBlogService>().To<BlogService>().InRequestScope();

            kernel.Bind<IService<BE.Entities.User>>().To<EntityService<BE.Entities.User, User>>().InRequestScope();
            kernel.Bind<IUserService>().To<UserService>().InRequestScope();
        }
    }
}
