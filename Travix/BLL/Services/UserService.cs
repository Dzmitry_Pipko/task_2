﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using DAL.Abstractions;
using AutoMapper;
using BLL.Abstractions;
using BLL.Helper;

using User = BE.DomainModels.User;

namespace BLL.Services
{
    public class UserService : EntityService<User>, IUserService
    {
        private const string Salt = "932DA01F-9ABD-9d9d-80C7-02AF85C02A8";

        IUserRepository _iUserRepository;

        public UserService(IUnitOfWork unitOfWork, IUserRepository iUserRepository)
            : base(unitOfWork, iUserRepository)
        {
            _iUserRepository = iUserRepository;
        }


        public override int Create(User entity)
        {
            entity.Password = AlgorithmHelper.GenerateHashWithSalt(entity.Password, Salt);

            return base.Create(entity);
        }

        public bool IsFreeLogin(string login)
        {
            throw new NotImplementedException();
        }

        public bool Login(string login, string password)
        {
            password = AlgorithmHelper.GenerateHashWithSalt(password, Salt);
            return _iUserRepository.Login(login, password);
        }

        public User GetUserByLogin(string login)
        {
            return _iUserRepository.GetUserByLogin(login);
        }
    }
}
