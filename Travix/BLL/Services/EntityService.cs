﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DAL.Abstractions;
using AutoMapper;
using BE.DomainModels;
using BLL.Abstractions;

namespace BLL.Services
{
    public abstract class EntityService<TDomainModel> : IService<TDomainModel> where TDomainModel : BaseDomainModel
    {
        protected IUnitOfWork _unitOfWork;
        IRepository<TDomainModel> _repository;

        public EntityService(IUnitOfWork unitOfWork, IRepository<TDomainModel> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public virtual int Create(TDomainModel entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            var ent = _repository.Add(entity);
            _unitOfWork.Commit();

            return ent.Id;
        }

        public virtual void Delete(TDomainModel entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _repository.Delete(entity);
            _unitOfWork.Commit();
        }

        public virtual IEnumerable<TDomainModel> GetAll()
        {
            return _repository.GetAll();
        }

        public TDomainModel Update(TDomainModel entity)
        {
            throw new NotImplementedException();
        }

        public TDomainModel GetById(int id)
        {
            return _repository.FindById(id);
        }
    }
}
