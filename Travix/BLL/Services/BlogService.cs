﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Abstractions;
using AutoMapper;
using BE.DomainModels;
using BLL.Abstractions;

namespace BLL.Services
{
    public class BlogService : EntityService<Blog>, IBlogService
    {
        IUserRepository _iUserRepository;

        public BlogService(IUnitOfWork unitOfWork, IBlogRepository iBlogRepository, IUserRepository iUserRepository)
            : base(unitOfWork, iBlogRepository)
        {
            _iUserRepository = iUserRepository;
        }

        public IEnumerable<Blog> GetBlogsByUserId(int id)
        {
            throw new NotImplementedException();
        }
    }
}
