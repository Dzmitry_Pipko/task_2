﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BE.DomainModels;
using BLL.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Travix.Controllers;
using Travix.ModelConverter;
using Travix.Models;

namespace Travix.Tests
{
    [TestClass]
    public class UserControllerTest
    {
        private IUserService _iUserService;

        [TestInitialize]
        public void Init()
        {
            AutoMapperConfiguration.Configure();
            _iUserService = MockRepository.GenerateStub<IUserService>();
        }

        [TestMethod]
        public void SigInModelError()
        {
            UserController controller = new UserController(_iUserService);
            controller.ModelState.AddModelError("someErrorKey", "Message");

            ViewResult result = controller.SignIn(new AccountUser(), null) as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("SignIn", result.ViewName);
        }

//        [TestMethod]
//        public void SigInUserIsNotFound()
//        {
//            AccountUser acountUser = new AccountUser()
//            {
//                Login = "Login",
//                Password = "Password"
//            };
//
//
//            //TODO stubs should be located in the base class (ControllerTest) for all tested controllers
//
//            var httpServerUtilityBase = MockRepository.GenerateStub<HttpServerUtilityBase>();
//            httpServerUtilityBase.Stub(ut => ut.HtmlEncode(Arg<string>.Is.Anything)).Return("somevalue");
//
//            var context = MockRepository.GenerateStub<HttpContextBase>();
//            context.Stub(x => x.Server).Return(httpServerUtilityBase);
//
//            var controllerContextMock = MockRepository.GenerateMock<ControllerContext>();
//            controllerContextMock.Stub(con => con.HttpContext).Return(context);
//
//            _iUserService.Stub(u => u.Login(Arg<string>.Is.Anything, Arg<string>.Is.Anything)).Return(false);
//            UserController controller = new UserController(_iUserService);
//
//            controller.ControllerContext = controllerContextMock;
//
//            ViewResult result = controller.SignIn(new AccountUser(), null) as ViewResult;
//
//            Assert.IsNotNull(result);
//            Assert.AreEqual("SignIn", result.ViewName);
//        }
    }
}
