﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.DomainModels
{
    public class Blog :BaseDomainModel
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}

