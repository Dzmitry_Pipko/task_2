﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BE.DomainModels;

namespace Abstractions
{
    public interface IRepositoryAsync
    {
        Task<string> GetMessage();
    }
}
