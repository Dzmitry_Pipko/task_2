﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Linq.Expressions;

namespace DAL.Abstractions
{
    public interface IRepository<T> 
    {
        IEnumerable<T> GetAll();
        T FindById(int id);
        T Add(T domainModel);
        bool Delete(T domainModel);
        void Edit(T domainModel);
        void Save();
    }
}
