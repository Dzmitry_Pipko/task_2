﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using BE.DomainModels;

namespace DAL.Abstractions
{
    public interface IUserRepository : IRepository<User>
    {
        bool Login(string login, string password);
        User GetUserByLogin(string login);
    }
}
