﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using BLL.Services.Interfaces;
using BE.Entities;
using Travix.Models;

namespace Travix.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        private readonly IUserService _iUserService;

        public UserController(IUserService iUserService)
        {
            _iUserService = iUserService;
        }

        [Authorize]
        public JsonResult Index()
        {
            var currentUser = _iUserService.GetUserByLogin(User.Identity.Name);
            return Json(currentUser, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken()] 
        public ActionResult SignIn(AccountUser user, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var domainUser = Mapper.Map<AccountUser, User>(user);

                domainUser.Login = Server.HtmlEncode(domainUser.Login);

                User authUser = _iUserService.Login(domainUser.Login, domainUser.Password);

                // User found in the database
                if (authUser != null)
                {
                    FormsAuthentication.SetAuthCookie(domainUser.Login, false);
                    return RedirectToAction("Index", "Home");
                }
              
                ModelState.AddModelError("CustomMessage", "The user name or password provided is incorrect.");
            }

            // If we got this far, something failed, redisplay form
            return View("SignIn");
        }

        public ActionResult SingOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}
