﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE.DomainModels;

namespace BLL.Abstractions
{
    public interface IBlogService : IService<Blog>
    {
        IEnumerable<Blog> GetBlogsByUserId(int id);
    }
}
