﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE.DomainModels;

namespace BLL.Abstractions
{
    public interface IService<T> where T: BaseDomainModel
    {
        int Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        T Update(T entity);
        T GetById(int id);
    }
}
