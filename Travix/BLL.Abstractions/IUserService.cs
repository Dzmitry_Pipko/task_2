﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE.DomainModels;

namespace BLL.Abstractions
{
    public interface IUserService : IService<User>
    {
        bool IsFreeLogin(string login);
        bool Login(string login, string password);
        User GetUserByLogin(string login);
    }
}
